<?php
/**
 * Implementation of hook_views_data()
 * (see Views module - Views 2 - let me know if when they document this properly!)
 */

function authorship_views_data() {
  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['node_authorship']['table']['group']  = t('Node');

  $data['node_authorship']['table']['base'] = array(
    'field' => 'authorship',
    'title' => t('Authorship'),
    'help' => t("Display the authorship setting for the node."),
  );

  //joins
  $data['node_authorship']['table']['join'] = array(
    //...to the node table
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  // ----------------------------------------------------------------
  // Fields

  // subject
  $data['node_authorship']['authorship'] = array(
    'title' => t('Authorship'),
    'help' => t('Display the author setting for the node.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  return $data;
}
?>