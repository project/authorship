README.txt file
===============

Admin >> Access Control
=======================

admin authorship
  Applying this perm to a role grants the following to users in that
  role (administrators) the ability to use this modules features on
  any given node.
  
user use authorship
  This allows a site administrator let regular users manage the 
  display of their own name (trusted users). Create a role and 
  set this perm to that role. Then, users in that role will be
  able to manage the display of their name in much the same way
  site administrators can but only for their own posts.

Admin >> Settings >> Content types
==================================

Enable authorship module functionality:
  Within the "workflow" frame, you now have the ability to enable or
  disable the authorship module functionality.

The profile variable name used to store the real name: 
  When the profile module is enabled a site admin can use it to gather
  additional variable data on a "per user" basis. You should enter in
  this text field the VARIABLE NAME that matches an already defined
  variable in the profile module. This will assumed to be the "Real Name"
  of the user. The value the user enters into the profile for this variable
  will be used for the display name on the node.
    
Node Forms
==========

All node forms come with standard options. One of these options is
"Authoring Information" drop down. The defaults are:-

  [AJAX] Authored by:
  Authored on:
  
This module adds, to admins and privileged users an additional field:-

  Authoeship display setting:
    This is the "Free Text" that authors can use to enter
    
    
    